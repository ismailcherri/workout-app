print('Creating Initial User #################################################################');
db = db.getSiblingDB('gymondo');
db.createUser(
    {
        user: 'gymondo_user',
        pwd: 'gymond_pass',
        roles: [{ role: 'readWrite', db: 'gymondo' }],
    },
);
print('END #################################################################');
