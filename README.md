## Description
This is a demo fullstack app to display a list page of workouts contains 
category and limit filters including a pager.
Clicking on any workout, leads to a workout details page.

## Structure

1. Backend: Written in NestJs typescript
2. Client: Written in ReactJs typescript using Redux-toolkit
3. Database: MongoDB provided as a Docker container

## Requirements
1. Docker and docker-compose
2. Node v14+

## Installation
To start the docker container, run the following from the root folder of the project:
```bash
$ docker-compose up
```

Then follow instructions in backend, then in client.

## Running the app
Follow the instructions in backend, then in client.


## Stay in touch

- Author - [Ismail Cherri](https://github.com/ismailcherri)
- Twitter - [@ismailcherri](https://twitter.com/ismailcherri)
