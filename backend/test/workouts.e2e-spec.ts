import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from 'src/app.module';
import { WorkoutsModule } from 'src/workouts/workouts.module';
import { Workout } from 'src/workouts/schema/workout.schema';
import * as dayjs from 'dayjs';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, WorkoutsModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    app.setGlobalPrefix('api');
    await app.init();
  });

  it('GET /api/workouts with no params should return 20 results', async () => {
    const result = await request(app.getHttpServer()).get('/api/workouts');
    expect(result.body.workouts).toBeDefined();
    expect(result.body.workouts).toBeInstanceOf(Array);
    expect(result.body.workouts.length).toBe(20);
  });

  it('GET /api/workouts?limit=2 limit the results', async () => {
    const result = await request(app.getHttpServer()).get(
      '/api/workouts?limit=2',
    );
    expect(result.body.workouts).toBeDefined();
    expect(result.body.workouts).toBeInstanceOf(Array);
    expect(result.body.workouts.length).toBe(2);
  });

  it('GET /api/workouts with no params should have page set to 1', async () => {
    const result = await request(app.getHttpServer()).get('/api/workouts');
    expect(result.body.page).toBeDefined();
    expect(result.body.page).toBe(1);
  });

  it('GET /api/workouts?page=3 should set the current page', async () => {
    const result = await request(app.getHttpServer()).get(
      '/api/workouts?page=3',
    );
    expect(result.body.page).toBeDefined();
    expect(result.body.page).toBe(3);
  });

  it('GET /api/workouts should return 400 if page is set to 0', async () => {
    const result = await request(app.getHttpServer()).get(
      '/api/workouts?page=0',
    );
    expect(result.status).toBe(400);
  });

  it('GET /api/workouts should have total and lastPage defined', async () => {
    const result = await request(app.getHttpServer()).get('/api/workouts');
    expect(result.body.total).toBeDefined();
    expect(result.body.lastPage).toBeDefined();
  });

  it('GET /api/workouts?category=c10 should return 400 for invalid category', async () => {
    const result = await request(app.getHttpServer()).get(
      '/api/workouts?category=c10',
    );
    expect(result.status).toBe(400);
  });

  it('GET /api/workouts?category=c1 should return workouts under c1 category', async () => {
    const result = await request(app.getHttpServer()).get(
      '/api/workouts?category=c1',
    );
    expect(result.status).toBe(200);
    result.body.workouts.forEach((workout: Workout) => {
      expect(workout.category).toBe('c1');
    });
  });

  it('GET /api/workouts?startDate=invalid should return 400 for invalid date', async () => {
    const result = await request(app.getHttpServer()).get(
      '/api/workouts?startDate=invalid',
    );
    expect(result.status).toBe(400);
  });

  it('GET /api/workouts?startDate=DATE should return future workouts only', async () => {
    let date = dayjs().add(30, 'days');
    const result = await request(app.getHttpServer()).get(
      `/api/workouts?startDate=${date.format('YYYY-MM-DD')}`,
    );
    expect(result.status).toBe(200);
    result.body.workouts.forEach((workout: Workout) => {
      expect(dayjs(workout.startDate).isAfter(date)).toBeTruthy();
    });
  });
});
