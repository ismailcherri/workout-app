## App backend

## Installation

```bash
$ npm install
```

## Running the app

* First time to generate database
```bash
$ npm run seed:workouts
```

* Running the dev environment
```bash
# development
$ npm run start

# watch mode
$ npm run start:dev
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Stay in touch

- Author - [Ismail Cherri](https://github.com/ismailcherri)
- Twitter - [@ismailcherri](https://twitter.com/ismailcherri)
