import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppService } from 'src/app.service';
import { AppController } from 'src/app.controller';
import { WorkoutsModule } from 'src/workouts/workouts.module';

@Module({
  imports: [
    WorkoutsModule,
    MongooseModule.forRoot(
      'mongodb://gymondo_user:gymond_pass@localhost:27017/gymondo?retryWrites=true&w=majority',
    ),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
