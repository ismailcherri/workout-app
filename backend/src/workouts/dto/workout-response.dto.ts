import { WorkoutDocument } from 'src/workouts/schema/workout.schema';

export interface WorkoutResponseDto {
  workouts: WorkoutDocument[];
  page: number;
  total: number;
  lastPage: number;
}
