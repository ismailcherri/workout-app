import { IsDateString, IsIn, IsInt, IsOptional, Min } from 'class-validator';
import { Categories, Category } from 'src/workouts/schema/workout.schema';
import { ApiPropertyOptional } from '@nestjs/swagger';
import * as dayjs from 'dayjs';
import { Type } from 'class-transformer';

export class OptionsDto {
  @IsInt()
  @IsOptional()
  @Type(() => Number)
  @ApiPropertyOptional({
    example: 20,
    description: 'Number of workouts per page',
    default: 20,
  })
  limit: number = 20;

  @IsInt()
  @Min(1)
  @Type(() => Number)
  @ApiPropertyOptional({
    example: 1,
    description: 'Sets the page number',
    minimum: 1,
  })
  page: number = 1;

  @IsOptional()
  @IsDateString()
  @ApiPropertyOptional({
    example: dayjs().format('YYYY-MM-DD'),
    description: 'Filter by the start date of the workout',
  })
  startDate: Date;

  @IsOptional()
  @IsIn(Object.keys(Categories))
  @ApiPropertyOptional({
    example: 'c1',
    description: `Must be value of ${Object.keys(Categories).toString()}`,
  })
  category: Category;
}
