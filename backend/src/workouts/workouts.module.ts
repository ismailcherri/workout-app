import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Workout, WorkoutSchema } from 'src/workouts/schema/workout.schema';
import { WorkoutsController } from 'src/workouts/workouts.controller';
import { WorkoutsService } from 'src/workouts/workouts.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Workout.name, schema: WorkoutSchema }]),
  ],
  controllers: [WorkoutsController],
  providers: [WorkoutsService],
})
export class WorkoutsModule {}
