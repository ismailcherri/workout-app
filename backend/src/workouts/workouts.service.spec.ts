import {Test, TestingModule} from '@nestjs/testing';
import {Model} from 'mongoose';
import {getModelToken} from '@nestjs/mongoose';
import sinon from 'ts-sinon';
import {WorkoutsService} from 'src/workouts/workouts.service';
import {Workout, WorkoutDocument} from 'src/workouts/schema/workout.schema';
import {OptionsDto} from 'src/workouts/dto/options.dto';

describe('WorkoutsService', () => {
  let service: WorkoutsService;
  let mockWorkoutModel: Model<WorkoutDocument>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        {
          provide: getModelToken(Workout.name),
          useValue: Model,
        },
        WorkoutsService,
      ],
    }).compile();

    mockWorkoutModel = module.get<Model<WorkoutDocument>>(
      getModelToken(Workout.name),
    );
    service = module.get<WorkoutsService>(WorkoutsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findOne', () => {
    it('should be defined', () => {
      jest
        .spyOn(mockWorkoutModel, 'findById')
        .mockResolvedValue({} as WorkoutDocument);
      expect(service.findOne('42')).toBeDefined();
    });

    it('should return one workout', async () => {
      const workout = new Workout();
      workout.name = 'best workout';
      const workoutId = '42';

      const spy = jest
        .spyOn(mockWorkoutModel, 'findById')
        .mockResolvedValue(workout as WorkoutDocument);

      const result = await service.findOne(workoutId);

      expect(spy).toBeCalled();
      expect(result).toBe(workout);
    });
  });

  describe('findAll', () => {
    it('should be defined', () => {
      const mock = sinon.mock(mockWorkoutModel);
      mock
        .expects('find')
        .twice()
        .returns({
          countDocuments: () => 0,
          limit: () => ({
            skip: () => ({
              exec: () => ({}),
            }),
          }),
        });
      expect(service.findAll({} as OptionsDto)).toBeDefined();
    });
  });
});
