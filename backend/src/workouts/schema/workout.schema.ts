import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Factory } from 'nestjs-seeder';
import { randomInt } from 'crypto';
import * as dayjs from 'dayjs';

export const Categories = {
  c1: 'c1',
  c2: 'c2',
  c3: 'c3',
  c4: 'c4',
  c5: 'c5',
  c6: 'c6',
  c7: 'c7',
};
export type Category = keyof typeof Categories;

export type WorkoutDocument = Workout & Document;

@Schema()
export class Workout {
  @Factory((faker) => faker.lorem.words(3))
  @Prop()
  name: string;

  @Factory((faker) => faker.lorem.words(20))
  @Prop()
  description: string;

  @Factory(
    () => Object.keys(Categories)[randomInt(0, Object.keys(Categories).length)],
  )
  @Prop()
  category: string;

  @Factory(() => dayjs().add(randomInt(2, 100), 'days').format())
  @Prop()
  startDate: Date;
}

export const WorkoutSchema = SchemaFactory.createForClass(Workout);
