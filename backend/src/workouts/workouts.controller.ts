import { Controller, Get, Param, Query } from '@nestjs/common';
import { WorkoutsService } from 'src/workouts/workouts.service';
import { OptionsDto } from 'src/workouts/dto/options.dto';

@Controller('workouts')
export class WorkoutsController {
  constructor(private readonly workoutsService: WorkoutsService) {}

  @Get()
  findAll(@Query() options: OptionsDto) {
    return this.workoutsService.findAll(options);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.workoutsService.findOne(id);
  }
}
