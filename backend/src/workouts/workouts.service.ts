import { Injectable } from '@nestjs/common';
import { Model, Query } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { OptionsDto } from 'src/workouts/dto/options.dto';
import { WorkoutResponseDto } from 'src/workouts/dto/workout-response.dto';
import { Workout, WorkoutDocument } from 'src/workouts/schema/workout.schema';

@Injectable()
export class WorkoutsService {
  constructor(
    @InjectModel(Workout.name)
    private readonly workoutModel: Model<WorkoutDocument>,
  ) {}

  async findAll(options: OptionsDto): Promise<WorkoutResponseDto> {
    const total = await this.getInitialQuery(options).countDocuments();

    const workouts = await this.getInitialQuery(options)
      .limit(options.limit)
      .skip((options.page - 1) * options.limit)
      .exec();

    const lastPage = Math.floor(total / options.limit);
    const page = options.page;
    return {
      workouts,
      total,
      lastPage,
      page,
    };
  }

  findOne(id: string): Query<WorkoutDocument, WorkoutDocument> {
    return this.workoutModel.findById(id);
  }

  private getInitialQuery(
    options: OptionsDto,
  ): Query<WorkoutDocument[], WorkoutDocument> {
    const query = this.workoutModel.find();
    if (options.category) {
      query.find({ category: options.category });
    }
    if (options.startDate) {
      query.find({ startDate: { $gt: options.startDate } });
    }

    return query;
  }
}
