import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { Model, Query } from 'mongoose';
import { WorkoutsController } from 'src/workouts/workouts.controller';
import { WorkoutsService } from 'src/workouts/workouts.service';
import { Workout, WorkoutDocument } from 'src/workouts/schema/workout.schema';
import { OptionsDto } from 'src/workouts/dto/options.dto';
import { WorkoutResponseDto } from 'src/workouts/dto/workout-response.dto';

describe('WorkoutsController', () => {
  let controller: WorkoutsController;
  let mockService: WorkoutsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WorkoutsController],
      providers: [
        {
          provide: getModelToken(Workout.name),
          useValue: Model,
        },
        WorkoutsService,
      ],
    }).compile();

    controller = module.get<WorkoutsController>(WorkoutsController);
    mockService = module.get<WorkoutsService>(WorkoutsService);

    jest
      .spyOn(mockService, 'findAll')
      .mockResolvedValue({} as WorkoutResponseDto);

    jest.spyOn(mockService, 'findOne').mockResolvedValue({} as WorkoutDocument);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(controller.findAll({} as OptionsDto)).toBeDefined();
    expect(controller.findOne('42')).toBeDefined();
  });
});
