import { DataFactory, Seeder } from 'nestjs-seeder';
import { InjectModel } from '@nestjs/mongoose';
import { Workout, WorkoutDocument } from 'src/workouts/schema/workout.schema';
import { Model } from 'mongoose';

export class WorkoutSeeder implements Seeder {
  constructor(
    @InjectModel(Workout.name)
    private readonly workoutModel: Model<WorkoutDocument>,
  ) {}

  async drop(): Promise<any> {
    return this.workoutModel.deleteMany({});
  }

  async seed(): Promise<any> {
    const workouts = DataFactory.createForClass(Workout).generate(1000);

    return this.workoutModel.insertMany(workouts);
  }
}
