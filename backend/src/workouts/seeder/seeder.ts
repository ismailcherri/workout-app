import { seeder } from 'nestjs-seeder';
import { MongooseModule } from '@nestjs/mongoose';
import { Workout, WorkoutSchema } from 'src/workouts/schema/workout.schema';
import { WorkoutSeeder } from 'src/workouts/seeder/workout.seeder';

seeder({
  imports: [
    MongooseModule.forFeature([{ name: Workout.name, schema: WorkoutSchema }]),
    MongooseModule.forRoot(
      'mongodb://gymondo_user:gymond_pass@localhost:27017/gymondo?retryWrites=true&w=majority',
    ),
  ],
}).run([WorkoutSeeder]);
