import { toQueryParams } from 'src/helper/query-builder';

describe('toQueryParam', () => {
  it('should convert object to query params', () => {
    const testObj = {
      prop: 1,
      anotherProp: 'anotherProp',
    };
    const expectedResult = 'prop=1&anotherProp=anotherProp';

    const result = toQueryParams(testObj);
    expect(result).toBe(expectedResult);
  });

  it('should omit null values in object', () => {
    const testObj = {
      prop: 1,
      anotherProp: null,
    };
    const expectedResult = 'prop=1';

    const result = toQueryParams(testObj);
    expect(result).toBe(expectedResult);
  });

  it('should omit undefined values in object', () => {
    const testObj = {
      prop: 1,
      anotherProp: undefined,
    };
    const expectedResult = 'prop=1';

    const result = toQueryParams(testObj);
    expect(result).toBe(expectedResult);
  });
});
