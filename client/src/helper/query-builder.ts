export function toQueryParams<T extends Object>(query: T): string {
  const searchParams = new URLSearchParams();
  Object.entries(query).forEach(
    (entry) => entry[1] && searchParams.append(entry[0], entry[1] as string),
  );
  return searchParams.toString();
}
