import { useAppDispatch, useAppSelector } from 'src/app/hooks';
import {
  fetchWorkOutsAsync,
  selectPageInformation,
  selectQueryParams,
} from 'src/features/workouts/workouts-slice';
import { useHistory } from 'react-router-dom';
import { toQueryParams } from 'src/helper/query-builder';
import React from 'react';
import ReactPaginate from 'react-paginate';

export function Pager() {
  const pageInformation = useAppSelector(selectPageInformation);
  const dispatch = useAppDispatch();
  let history = useHistory();
  let queryParams = useAppSelector(selectQueryParams);
  const handlePageChange = (value: number) => {
    queryParams = {
      ...queryParams,
      page: value,
    };
    history.push({
      pathname: '/',
      search: toQueryParams(queryParams),
    });
    dispatch(fetchWorkOutsAsync(queryParams));
  };

  const lastPage = pageInformation.lastPage || 1;

  return (
    <div>
      <ReactPaginate
        previousLabel={'previous'}
        nextLabel={'next'}
        breakLabel={'...'}
        breakClassName={'inline-block mx-4 p-4 border-2'}
        pageCount={lastPage}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={(number) => handlePageChange(number.selected + 1)}
        containerClassName={'block m-5'}
        activeClassName={'border-red-900'}
        pageClassName={'inline-block mx-4 p-4 border-2'}
        nextClassName={'inline-block mx-4 p-4 border-2'}
        previousClassName={'inline-block mx-4 p-4 border-2'}
      />
    </div>
  );
}
