import React, { ChangeEvent } from 'react';
import {
  fetchWorkOutsAsync,
  selectQueryParams,
} from 'src/features/workouts/workouts-slice';
import { toQueryParams } from 'src/helper/query-builder';
import { useAppDispatch, useAppSelector } from 'src/app/hooks';
import { useHistory } from 'react-router-dom';

export function LimitSelect() {
  const dispatch = useAppDispatch();
  let history = useHistory();
  let queryParams = useAppSelector(selectQueryParams);

  const handleLimitChange = (event: ChangeEvent<HTMLSelectElement>) => {
    queryParams = {
      ...queryParams,
      limit: !!event.target.value ? Number(event.target.value) : 20,
    };

    history.push({
      pathname: '/',
      search: toQueryParams(queryParams),
    });
    dispatch(fetchWorkOutsAsync(queryParams));
  };

  return (
    <select
      name="limit"
      id="limit"
      onChange={handleLimitChange}
      defaultValue={queryParams.limit}
    >
      <option value="5">5</option>
      <option value="10">10</option>
      <option value="15">15</option>
      <option value="20">20</option>
    </select>
  );
}
