import React from 'react';
import { Link } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from 'src/app/hooks';
import {
  fetchWorkOutsAsync,
  selectQueryParams,
  selectWorkouts,
} from 'src/features/workouts/workouts-slice';
import { CategorySelect } from 'src/app/components/category-select';
import { LimitSelect } from 'src/app/components/limit-select';
import { Pager } from 'src/app/components/pager';

export function WorkoutsPage() {
  const workouts = useAppSelector(selectWorkouts);
  let queryParams = useAppSelector(selectQueryParams);
  const dispatch = useAppDispatch();
  dispatch(fetchWorkOutsAsync(queryParams));

  return (
    <div>
      <div>
        <LimitSelect />
        <CategorySelect />
      </div>
      <div>
        {workouts?.map((work) => (
          <div key={work._id}>
            <Link to={`/workouts/${work._id}`}>
              <img src="https://picsum.photos/300/300" alt={work.name} />
              <p>{work.name}</p>
              <p>{work.description}</p>
              <p>{work.category}</p>
            </Link>
          </div>
        ))}
      </div>
      <div>
        <Pager />
      </div>
    </div>
  );
}
