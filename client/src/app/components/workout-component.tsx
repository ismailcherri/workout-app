import {
  fetchWorkOutByIdAsync,
  selectWorkout,
} from 'src/features/workouts/workouts-slice';
import React, { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from 'src/app/hooks';
import { useParams } from 'react-router-dom';

export function Workout() {
  const dispatch = useAppDispatch();
  const work = useAppSelector(selectWorkout);
  let { id } = useParams<{ id: string }>();

  useEffect(() => {
    dispatch(fetchWorkOutByIdAsync(id));
  }, [dispatch, id]);

  return (
    <div>
      <img src="https://picsum.photos/300/300" alt={work.name} />
      <p>{work.name}</p>
      <p>{work.description}</p>
      <p>{work.category}</p>
    </div>
  );
}
