import React, { ChangeEvent } from 'react';
import {
  CategoryArray,
  CategoryType,
  fetchWorkOutsAsync,
  selectQueryParams,
} from 'src/features/workouts/workouts-slice';
import { toQueryParams } from 'src/helper/query-builder';
import { useAppDispatch, useAppSelector } from 'src/app/hooks';
import { useHistory } from 'react-router-dom';

export function CategorySelect() {
  const dispatch = useAppDispatch();
  let history = useHistory();
  let queryParams = useAppSelector(selectQueryParams);

  const handleCategoryChange = (event: ChangeEvent<HTMLSelectElement>) => {
    queryParams = {
      ...queryParams,
      category: !!event.target.value
        ? (event.target.value as CategoryType)
        : undefined,
    };
    history.push({
      pathname: '/',
      search: toQueryParams(queryParams),
    });
    dispatch(fetchWorkOutsAsync(queryParams));
  };

  return (
    <select
      name="category"
      id="category"
      className="border-solid border border-2 rounded-lg"
      onChange={handleCategoryChange}
      defaultValue={queryParams.category}
    >
      <option value="">Select</option>
      {CategoryArray.map((category) => (
        <option key={category} value={category}>
          {category}
        </option>
      ))}
    </select>
  );
}
