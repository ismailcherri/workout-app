import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import workoutReducer from 'src/features/workouts/workouts-slice';

export const store = configureStore({
  reducer: {
    workout: workoutReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
