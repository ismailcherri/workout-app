import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'src/app/store';
import {
  fetchWorkout,
  fetchWorkouts,
  WorkoutPayload,
} from 'src/features/workouts/workouts-api';

export const CategoryArray = [
  'c1',
  'c2',
  'c3',
  'c4',
  'c5',
  'c6',
  'c7',
] as const;
export type CategoryType = typeof CategoryArray[number];

export interface Workout {
  _id: string;
  startDate?: Date;
  category?: CategoryType;
  description?: string;
  name?: string;
}

export interface WorkoutResponse {
  workouts: Workout[];
  total?: number;
  lastPage?: number;
  page?: number;
}

export interface WorkoutState {
  activeWorkout: Workout;
  response: WorkoutResponse;
  status: 'idle' | 'loading' | 'failed' | 'success';
  activeCategory?: CategoryType;
  queryParams: WorkoutPayload;
}

const initialState: WorkoutState = {
  activeWorkout: { _id: '' },
  response: {
    workouts: [],
  },
  status: 'idle',
  queryParams: {
    limit: 20,
    category: undefined,
    page: 1,
  },
};

export const fetchWorkOutsAsync = createAsyncThunk(
  'workout/fetchWorkouts',
  async (payload: WorkoutPayload) => {
    const response = await fetchWorkouts(payload);

    return response.data as WorkoutResponse;
  },
  {
    condition(arg: WorkoutPayload, api): boolean | undefined {
      const rootState = api.getState() as RootState;
      const response = rootState.workout.response;
      return (
        arg.limit !== response.workouts.length ||
        arg.page !== response.page ||
        arg.category !== rootState.workout.activeCategory
      );
    },
  },
);

export const fetchWorkOutByIdAsync = createAsyncThunk(
  'workout/fetchWorkoutByIds',
  async (id: string) => {
    const response = await fetchWorkout(id);

    return response.data as Workout;
  },
  {
    condition(arg: string, api): boolean | undefined {
      const rootState = api.getState() as RootState;
      return arg !== rootState.workout.activeWorkout._id;
    },
  },
);

export const workoutSlice = createSlice({
  name: 'workout',
  initialState,
  reducers: {
    updateQueryParams(state, action: PayloadAction<WorkoutPayload>) {
      state.queryParams = Object.assign({}, action.payload);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchWorkOutsAsync.pending, (state) => {
      state.status = 'loading';
    });
    builder.addCase(fetchWorkOutsAsync.fulfilled, (state, action) => {
      state.activeCategory = action.meta.arg.category;
      state.queryParams = Object.assign({}, action.meta.arg);

      state.response.workouts = action.payload.workouts;
      state.response.page = action.payload.page;
      state.response.total = action.payload.total;
      state.response.lastPage = action.payload.lastPage;
      state.status = 'idle';
    });
    builder.addCase(fetchWorkOutByIdAsync.pending, (state) => {
      state.status = 'loading';
    });
    builder.addCase(fetchWorkOutByIdAsync.fulfilled, (state, action) => {
      state.activeWorkout = action.payload;
      state.status = 'idle';
    });
  },
});

export const selectWorkouts = (state: RootState) =>
  state.workout.response?.workouts;

export const selectPageInformation = (state: RootState) => ({
  currentPage: state.workout.response?.page,
  lastPage: state.workout.response?.lastPage,
  total: state.workout.response?.total,
});

export const selectWorkout = (state: RootState) => state.workout.activeWorkout;
export const selectStatus = (state: RootState) => state.workout.status;
export const selectQueryParams = (state: RootState) =>
  state.workout.queryParams;
export default workoutSlice.reducer;
