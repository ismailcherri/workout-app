import { CategoryType } from 'src/features/workouts/workouts-slice';
import axios from 'axios';

export const httpClient = axios.create({
  baseURL: 'http://localhost:4200/api',
});

export interface WorkoutPayload {
  limit: number;
  category?: CategoryType;
  page?: number;
}

export function fetchWorkouts(payload: WorkoutPayload) {
  return httpClient.get('/workouts', {
    params: {
      limit: payload.limit,
      category: payload.category,
      page: payload.page || 1,
    },
  });
}

export function fetchWorkout(id: string) {
  return httpClient.get(`/workouts/${id}`);
}
