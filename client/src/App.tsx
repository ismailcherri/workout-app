import React from 'react';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';
import { Workout } from 'src/app/components/workout-component';
import { WorkoutsPage } from 'src/app/components/workouts-page-component';

function App() {
  return (
    <div className="App antialiased font-sans container mx-auto">
      <BrowserRouter>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
        </ul>
        <Switch>
          <Route exact path="/">
            <WorkoutsPage />
          </Route>
          <Route path="/workouts/:id">
            <Workout />
          </Route>
          <Route path="*">404</Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
