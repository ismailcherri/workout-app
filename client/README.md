## App Frontend

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start
```

## Runing the tests
```bash
# watch mode
$ npm run test
```

## Stay in touch

- Author - [Ismail Cherri](https://github.com/ismailcherri)
- Twitter - [@ismailcherri](https://twitter.com/ismailcherri)
